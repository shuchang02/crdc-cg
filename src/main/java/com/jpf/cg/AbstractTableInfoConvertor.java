package com.jpf.cg;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public abstract class AbstractTableInfoConvertor implements TableInfoConvertor {
	
	protected Connection conn;
	protected String tableName;
	
	public AbstractTableInfoConvertor() throws ClassNotFoundException, IOException, SQLException {
		this("jdbc.properties");
	}
	
	public AbstractTableInfoConvertor(String jdbcPropertiesFile) throws ClassNotFoundException, IOException, SQLException {
		Properties jdbcConfig = PropertiesLoaderUtils.loadProperties(new ClassPathResource(jdbcPropertiesFile));
		Class.forName(jdbcConfig.getProperty("driverClass"));
		conn = DriverManager.getConnection(
				jdbcConfig.getProperty("url"), jdbcConfig.getProperty("username"), jdbcConfig.getProperty("password"));
	}
	
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String toEntityName(String tableName) {
		tableName = tableName.toLowerCase().substring(1);
		boolean flag = false;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < tableName.length(); i++) {
			char w = tableName.charAt(i);
			if(w == '_') {
				flag = true;
				continue;
			}
			if(flag) {
				sb.append(Character.toUpperCase(w));
				flag = false;
			} else {
				sb.append(w);
			}
		}
		return sb.toString();
	}
	
	public String toProperty(String column) {
		column = column.toLowerCase();
		boolean flag = false;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < column.length(); i++) {
			char w = column.charAt(i);
			if(w == '_') {
				flag = true;
				continue;
			}
			if(flag) {
				sb.append(Character.toUpperCase(w));
				flag = false;
			} else {
				sb.append(w);
			}
		}
		return sb.toString();
	}
	
	public String toGetter(String property) {
		return toGS(property, "get");
	}

	public String toSetter(String property) {
		return toGS(property, "set");
	}

	private String toGS(String property, String prefix) {
		char w = Character.toUpperCase(property.charAt(0));
		return prefix + w + property.substring(1);
	}
	
}

package com.jpf.cg.gen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.jpf.cg.TableInfoConvertor;

public abstract class Generator {
	
	protected TableInfoConvertor convertor;
	protected Properties config;
	
	public Generator(TableInfoConvertor convertor) throws IOException {
		this.convertor = convertor;
		config = PropertiesLoaderUtils.loadProperties(new ClassPathResource("generator.properties"));
	}
	
	public abstract void generate() throws SQLException, IOException;
	
	protected String getTempContent() throws IOException {
		URL url = new ClassPathResource(getTemplateFile()).getURL();
		String content = IOUtils.toString(url);
		return StringUtils.replaceOnce(content, "#{author}", config.getProperty("author"));
	}
	
	protected void write(String content, String filename) throws IOException {
		System.out.println("正在生成文件... " + getOutput(filename));
		FileOutputStream os = new FileOutputStream(new File(getOutput(filename)));
		IOUtils.write(content, os, "utf-8");
		os.close();
	}
	
	protected String getTemplateFile() {
		return config.getProperty("template." + getSuffixKey());
	}
	
	protected String getPackage() {
		return config.getProperty("package." + getSuffixKey());
	}
	
	protected String getPackage(String subpackage) {
		return config.getProperty("package." + getSuffixKey()) + "." + subpackage;
	}
	
	protected String getOutput(String filename) {
		String root = config.getProperty("projectPath");
		String path = root + config.getProperty("output." + getSuffixKey());
		new File(path).mkdirs();
		return path + filename;
	}
	
	/**
	 * generator.properties文件里的属性后缀，如template.controller，则返回controller，
	 * template.dao.table则返回dao.table
	 * @return
	 */
	protected abstract String getSuffixKey();
	
	/**
	 * 对象名的后缀，如UserService则返回Service
	 * @return
	 */
	protected String getSuffix() {
		return config.getProperty("suffix." + getSuffixKey(), "");
	}
}

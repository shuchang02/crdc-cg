package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;
import com.jpf.cg.utils.Kit;

public class GenController extends Generator {

	private String subPackage;

	/**
	 * @param convertor
	 * @param subPackage 如果你的controller在子包里，请提供子包路径，如："admin"或"service.admin"
	 * @throws IOException
	 */
	public GenController(TableInfoConvertor convertor, String subPackage) throws IOException {
		super(convertor);
		this.subPackage = subPackage;
	}
	
	public GenController(TableInfoConvertor convertor) throws IOException {
		this(convertor, null);
	}

	@Override
	public void generate() throws SQLException, IOException {
		
		TableMapper tm = convertor.getTableMapper();
		
		String content = super.getTempContent();
		content = StringUtils.replaceOnce(content, "#{package}", getPackage(this.subPackage));
		content = StringUtils.replaceOnce(content, "#{entityPackage}", 
				config.getProperty("package.entity") + "." + tm.entityName);
		content = StringUtils.replaceOnce(content, "#{servicePackage}", 
				config.getProperty("package.service") + "." + tm.entityName + config.getProperty("suffix.service"));
		content = StringUtils.replace(content, "#{projectAlias}", config.getProperty("projectAlias"));
		content = StringUtils.replace(content, "#{entity}", tm.entityName);
		content = StringUtils.replace(content, "#{entityVar}", tm.getEntityVar());
		
		
		String requestMapping = tm.getEntityVar();
		String controllerName = "";
		if(StringUtils.isNotBlank(this.subPackage)) {
			requestMapping = this.subPackage.replaceAll("\\.", "/") + "/" + tm.getEntityVar();
			String tmp = "";
			for(String t : StringUtils.split(this.subPackage, ".")) {
				tmp += new Character(Character.toUpperCase(t.charAt(0))).toString() + t.substring(1);
			}
			controllerName = String.format("(\"%s\")", tmp + tm.entityName + "Controller");
		}
		content = StringUtils.replace(content, "#{controller_name}", controllerName);
		content = StringUtils.replace(content, "#{request_mapping}", requestMapping.toLowerCase());
		content = StringUtils.replace(content, "#{index_view_path}", Kit.convertNameTo_(requestMapping));
		
		String path = StringUtils.isBlank(this.subPackage) ? "" : this.subPackage + "/";
		super.write(content, path + tm.entityName + getSuffix() + ".java");
	}

	@Override
	protected String getSuffixKey() {
		return "controller";
	}
}

package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.PropertyMapper;
import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;

public class GenDao extends Generator {


	public GenDao(TableInfoConvertor convertor) throws IOException {
		super(convertor);
	}

	@Override
	public void generate() throws SQLException, IOException {
		
		TableMapper tm = convertor.getTableMapper();
		List<PropertyMapper> pms = convertor.getPropertiesMapper();
		
		String content = super.getTempContent();
		content = StringUtils.replaceOnce(content, "#{package}", getPackage());
		content = StringUtils.replaceOnce(content, "#{mapperPackage}", 
				config.getProperty("package.entity.mapper") + "." + tm.entityName + config.getProperty("suffix.entity.mapper"));
		content = StringUtils.replaceOnce(content, "#{tablePackage}", 
				config.getProperty("package.entity.table") + "." + tm.entityName + config.getProperty("suffix.entity.table"));
		content = StringUtils.replaceOnce(content, "#{entityPackage}", 
				config.getProperty("package.entity") + "." + tm.entityName);
		content = StringUtils.replace(content, "#{entity}", tm.entityName);
		
		StringBuilder sb = new StringBuilder();
		for(PropertyMapper pm : pms) {
			sb.append(String.format("\t\tormMapper.put(\"%s\", %s);", pm.property, pm.column.toUpperCase()));
			sb.append('\n');
		}
		content = StringUtils.replaceOnce(content, "#{ormMapper}", sb.toString());
		
		super.write(content, tm.entityName + getSuffix() + ".java");
	}

	@Override
	protected String getSuffixKey() {
		return "dao";
	}
	
	
}

package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;

public class GenService extends Generator {


	public GenService(TableInfoConvertor convertor) throws IOException {
		super(convertor);
	}

	@Override
	public void generate() throws SQLException, IOException {
		
		TableMapper tm = convertor.getTableMapper();
		
		String content = super.getTempContent();
		content = StringUtils.replaceOnce(content, "#{package}", config.getProperty("package.service"));
		content = StringUtils.replaceOnce(content, "#{daoPackage}", 
				config.getProperty("package.dao") + "." + tm.entityName + config.getProperty("suffix.dao"));
		content = StringUtils.replaceOnce(content, "#{entityPackage}", 
				config.getProperty("package.entity") + "." + tm.entityName);
		content = StringUtils.replace(content, "#{entity}", tm.entityName);
		content = StringUtils.replace(content, "#{entityVar}", tm.getEntityVar());
		
		super.write(content, tm.entityName + "Service.java");
	}

	@Override
	protected String getSuffixKey() {
		return "service";
	}
	
	
}

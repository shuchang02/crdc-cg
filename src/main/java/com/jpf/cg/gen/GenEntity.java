package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.PropertyMapper;
import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;

public class GenEntity extends Generator {


	public GenEntity(TableInfoConvertor convertor) throws IOException {
		super(convertor);
	}

	@Override
	public void generate() throws SQLException, IOException {
		
		TableMapper tm = convertor.getTableMapper();
		List<PropertyMapper> pms = convertor.getPropertiesMapper();
		
		String content = super.getTempContent();
		content = StringUtils.replaceOnce(content, "#{package}", config.getProperty("package.entity"));
		content = StringUtils.replaceOnce(content, "#{classComment}", tm.comment);
		content = StringUtils.replaceOnce(content, "#{entityName}", tm.entityName);
		
		StringBuilder properties = new StringBuilder();
		StringBuilder gser = new StringBuilder();
		StringBuilder imports = new StringBuilder();
		for(PropertyMapper pm : pms) {
			if(pm.property.equals("id")) continue;
			String javaType = convertor.toJavaType(pm.sqlType);
			
			//import
			if("Date".equals(javaType) && !StringUtils.contains(imports.toString(), "java.util.Date")) {
				imports.append("import java.util.Date;");
				imports.append('\n');
			}
			
			//property
			if(StringUtils.isNoneBlank(pm.comment)) {
				properties.append(String.format("\t//%s", pm.comment));
				properties.append('\n');
			}
			properties.append(String.format("\tprivate %s %s;", javaType, pm.property));
			properties.append('\n');
			
			//getter and setter
			gser.append(String.format("\tpublic %s %s() {\n\t\treturn %s;\n\t}", 
					javaType, convertor.toGetter(pm.property), pm.property));
			gser.append('\n');
			gser.append('\n');
			gser.append(String.format("\tpublic void %s(%s %s) {\n\t\tthis.%s = %s;\n\t}", 
					convertor.toSetter(pm.property), javaType, pm.property, pm.property, pm.property));
			gser.append('\n');
			gser.append('\n');
		}
		properties.append('\n');
		content = StringUtils.replaceOnce(content, "#{import}", imports.toString());
		content = StringUtils.replaceOnce(content, "#{properties}", properties.toString() + gser.toString());
		
		
		super.write(content, tm.entityName + ".java");
	}

	@Override
	protected String getSuffixKey() {
		return "entity";
	}
	
	
}

package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.PropertyMapper;
import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;

public class GenEntityTable extends Generator {
	
	public GenEntityTable(TableInfoConvertor convertor) throws IOException {
		super(convertor);
	}
	
	@Override
	public void generate() throws SQLException, IOException {
		TableMapper tm = convertor.getTableMapper();
		List<PropertyMapper> pms = convertor.getPropertiesMapper();
		
		String content = super.getTempContent();
		content = StringUtils.replaceOnce(content, "#{package}", config.getProperty("package.entity.table"));
		content = StringUtils.replaceOnce(content, "#{author}", config.getProperty("author"));
		content = StringUtils.replaceOnce(content, "#{entityName}", tm.entityName);
		
		content = StringUtils.replaceOnce(content, "#{tableName}", "\tpublic static final String TABLE_NAME = \"" + tm.tableName + "\";");
		
		StringBuilder sb = new StringBuilder('\n');
		for(PropertyMapper pm : pms) {
			sb.append(String.format("\tpublic static final String %s = \"%s\";", pm.column, pm.column));
			sb.append('\n');
		}
		content = StringUtils.replaceOnce(content, "#{columns}", sb.toString());
		
		
		super.write(content, tm.entityName + "Table.java");
	}

	@Override
	protected String getSuffixKey() {
		return "entity.table";
	}

}

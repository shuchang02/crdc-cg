package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.PropertyMapper;
import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;
import com.jpf.cg.utils.Kit;

public class GenJS extends Generator {

	private String subPackage;
	
	public GenJS(TableInfoConvertor convertor, String subPackage) throws IOException {
		super(convertor);
		this.subPackage = subPackage;
	}

	@Override
	public void generate() throws SQLException, IOException {
		
		TableMapper tm = convertor.getTableMapper();
		List<PropertyMapper> pms = convertor.getPropertiesMapper();
		
		String content = super.getTempContent();
		
		StringBuilder properties = new StringBuilder();
		int i = 0;
		for(PropertyMapper pm : pms) {
			i++;
			if(pm.property.equals("id")) continue;
			properties.append("\t\t\t\t");
			String s = "";
			if(i != pms.size()) {
				s = ",";
			}
			properties.append(String.format("{field: '%s', title: '%s'}%s", pm.property, pm.comment, s));
			properties.append("\n");
		}
		if(StringUtils.isBlank(this.subPackage)) {
			this.subPackage = "";
		}
		content = StringUtils.replaceOnce(content, "#{baseurl}", "/" + this.subPackage.replaceAll("\\.", "/") + "/" + Kit.convertNameTo_(tm.getEntityVar()));
		content = StringUtils.replaceOnce(content, "#{properties}", properties.toString());
		
		String path = StringUtils.isBlank(this.subPackage) ? "" : this.subPackage + "/";
		super.write(content, path + Kit.convertNameTo_(tm.getEntityVar()) + ".js");
	}

	@Override
	protected String getSuffixKey() {
		return "js";
	}
	
	
}

package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.PropertyMapper;
import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;

public class GenEntityMapper extends Generator {

	public GenEntityMapper(TableInfoConvertor convertor) throws IOException {
		super(convertor);
	}
	
	@Override
	public void generate() throws SQLException, IOException {
		TableMapper tm = convertor.getTableMapper();
		List<PropertyMapper> pms = convertor.getPropertiesMapper();
		String content = super.getTempContent();
		content = StringUtils.replaceOnce(content, "#{package}", config.getProperty("package.entity.mapper"));
		content = StringUtils.replaceOnce(content, "#{tablePackage}", 
				config.getProperty("package.entity.table") + "." + tm.entityName + config.getProperty("suffix.entity.table"));
		content = StringUtils.replaceOnce(content, "#{entityPackage}", 
				config.getProperty("package.entity") + "." + tm.entityName);
		content = StringUtils.replace(content, "#{entity}", tm.entityName);
		StringBuilder sb = new StringBuilder();
		for(PropertyMapper pm : pms) {
			if(pm.property.equals("id")) continue;
			String javaType = convertor.toJavaType(pm.sqlType);
			if("Integer".equals(javaType)) {
				javaType = "Int";
			}
			sb.append(String.format("\t\tbean.%s(rs.get%s(%s));", 
					convertor.toSetter(pm.property), javaType, pm.column));
			sb.append('\n');
		}
		content = StringUtils.replaceOnce(content, "#{setter}", sb.toString());
		super.write(content, tm.entityName + "Mapper.java");
	}

	@Override
	protected String getSuffixKey() {
		return "entity.mapper";
	}

}

package com.jpf.cg.gen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jpf.cg.PropertyMapper;
import com.jpf.cg.TableInfoConvertor;
import com.jpf.cg.TableMapper;
import com.jpf.cg.utils.Kit;

public class GenJade extends Generator {

	private String subPackage;

	public GenJade(TableInfoConvertor convertor) throws IOException {
		this(convertor, null);
	}
	
	/**
	 * @param convertor
	 * @param subPackage
	 * @throws IOException
	 */
	public GenJade(TableInfoConvertor convertor, String subPackage) throws IOException {
		super(convertor);
		this.subPackage = subPackage;
	}

	@Override
	public void generate() throws SQLException, IOException {
		
		TableMapper tm = convertor.getTableMapper();
		List<PropertyMapper> pms = convertor.getPropertiesMapper();
		
		String content = super.getTempContent();
		String subpath = "";
		if(StringUtils.isNotBlank(this.subPackage)) {
			subpath = this.subPackage.replaceAll("\\.", "/") + "/";
		}
		content = StringUtils.replace(content, "#{js_path}", String.format("/assets/js/app/%s%s.js", subpath, Kit.convertNameTo_(tm.getEntityVar())));
		
		StringBuilder formitem = new StringBuilder();
		for(PropertyMapper pm : pms) {
			if(pm.property.equals("id")) continue;
			formitem.append("\t\t\t\t.item\n");
			formitem.append("\t\t\t\t\tlabel.field " + pm.comment + "：\n");
			formitem.append("\t\t\t\t\t// - 字段类型：" + pm.sqlType + "，请修改验证规则后将此注释删除\n");
			formitem.append(String.format("\t\t\t\t\tinput.easyui-textbox(name='%s')\n", pm.property));
			formitem.append("\n");
		}
		content = StringUtils.replaceOnce(content, "#{formitem}", formitem.toString());
		
		String path = StringUtils.isBlank(this.subPackage) ? "" : this.subPackage + "/";
		super.write(content, path + Kit.convertNameTo_(tm.getEntityVar()) + ".jade");
	}

	@Override
	protected String getSuffixKey() {
		return "jade";
	}
	
	
}

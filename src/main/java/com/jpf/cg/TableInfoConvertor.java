package com.jpf.cg;

import java.sql.SQLException;
import java.util.List;

public interface TableInfoConvertor {

	public TableMapper getTableMapper() throws SQLException;
	
	public List<PropertyMapper> getPropertiesMapper() throws SQLException;
	
	public String toJavaType(String sqlsType);
	
	public String toGetter(String property);
	
	public String toSetter(String property);
}

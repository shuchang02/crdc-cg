package com.jpf.cg;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class MySQLConvertor extends AbstractTableInfoConvertor {
	
	public MySQLConvertor(String tableName) throws ClassNotFoundException, IOException, SQLException {
		super();
		this.tableName = tableName;
	}

	public TableMapper getTableMapper() throws SQLException {
		Statement s = conn.createStatement();
		ResultSet rs = s.executeQuery(String.format("show table status where name='%s'", tableName));
		rs.next();
		String comment = StringUtils.defaultString(rs.getString("comment"));
		rs.close();
		TableMapper mapper = new TableMapper();
		mapper.comment = comment;
		mapper.tableName = tableName.toUpperCase();
		mapper.entityName = super.toEntityName(tableName);
		return mapper;
	}

	public List<PropertyMapper> getPropertiesMapper() throws SQLException {
		List<PropertyMapper> mapper = new LinkedList<PropertyMapper>();
		Statement s = conn.createStatement();
		ResultSet rs = s.executeQuery("show full columns from " + tableName);
		while(rs.next()) {
			PropertyMapper pm = new PropertyMapper();
			pm.sqlType = rs.getString("type");
			pm.column = rs.getString("field");
			pm.comment = rs.getString("comment");
			pm.property = super.toProperty(pm.column);
			mapper.add(pm);
		}
		return mapper;
	}

	public String toJavaType(String sqlType) {
		if(Pattern.matches(".*char.*?", sqlType)) {
			return "String";
		} else if(Pattern.matches("(int|tinyint|integer|smallint|mediumint)[\\(\\),0-9]*?", sqlType)) {
			return "Integer";
		} else if(Pattern.matches("date|time|datetime|timestamp", sqlType)) {
			return "Date";
		} else if(Pattern.matches("(double|decimal|numeric)[\\(\\),0-9]*?", sqlType)) {
			return "Double";
		} else if(Pattern.matches("float[\\(\\),0-9]*?", sqlType)) {
			return "Float";
		} else {
			throw new RuntimeException("unknow type " + sqlType);
		}
	}
}

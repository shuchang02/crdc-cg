package com.jpf.cg.utils;


public class Kit {

	/**
	 * 将驼峰样式名称转为下划线分割，如：UserName -> user_name
	 * @param name
	 * @return
	 */
	public static String convertNameTo_(String name) {
		if(name == null) return name;
		String f = new Character(Character.toLowerCase(name.charAt(0))).toString();
		name = f + name.substring(1);
		for(int i = 65; i <= 90; i++) {
			String s = new Character((char)i).toString();
			name = name.replaceAll(s, "_" + s.toLowerCase());
		}
		return name;
	}
}

package com.jpf.cg;

public class TableMapper {

	public String tableName;
	public String entityName;
	public String comment;
	
	public String getEntityVar() {
		String w = entityName.substring(0, 1).toLowerCase();
		return w + entityName.substring(1);
	}
}
